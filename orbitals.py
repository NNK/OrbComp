import math
import sqlite3 as sql

conn = sql.connect('planetData.db')

class CelestialBody():
    def __init__(self, name, db_conn):
        self.name = name
        self.c = db_conn.cursor()

    def Mass(self):
        cmd = "SELECT mass FROM objects WHERE name='%s'" % self.name
        self.c.execute(cmd)
        mass = int(self.c.fetchone()[0])
        return mass
    
    def Radius(self):
        cmd = "SELECT radius FROM objects WHERE name='%s'" % self.name
        self.c.execute(cmd)
        radius = int(self.c.fetchone()[0])
        return radius

    def GM(self):
        cmd = "SELECT grav FROM objects WHERE name='%s'" % self.name
        self.c.execute(cmd)
        gm = int(self.c.fetchone()[0])
        return gm

    def SurfGrav(self):
        cmd = "SELECT surfaceGrav FROM objects WHERE name='%s'" % self.name
        self.c.execute(cmd)
        surfGrav = int(self.c.fetchone()[0])
        return surfGrav

    def OrbitalRadius(self):
        cmd = "SELECT orbitalRadius FROM objects WHERE name='%s'" & self.name
        self.c.execute(cmd)
        orbRad = int(self.c.fetchone()[0])
        return orbRad

class DeltaV():
    def orbitalInsert(self, originGM, originRad, originSurfGrav, orbitHeight):
        dv1 = (math.sqrt(2 * originSurfGrav * (orbitHeight * 1000))) / 1000
        dv2 = math.sqrt(originGM / (originRad + orbitHeight))
        dv = dv1 + dv2
        return dv

    def hohmannTransfer(self, starGM, originGM, targetGM, originRad, targetRad, originOrbit, targetOrbit, craftOriginOrbit, craftTargetOrbit):
        vHohmann = math.sqrt((2 * starGM * targetOrbit) / (originOrbit * (originOrbit + targetOrbit)))
        vOriginBody = math.sqrt(starGM / originOrbit)
        vRawTransfer = vHohmann - vOriginBody
        vEscape = math.sqrt((vRawTransfer**2) + ((2 * originGM) / (craftOriginOrbit + originRad)))
        vCraftOriginOrbit = math.sqrt(originGM / (craftOriginOrbit + originRad))
        dv1 = vEscape - vCraftOriginOrbit

        vHohmannArrival = math.sqrt((2 * starGM * originOrbit)/(targetOrbit * (originOrbit + targetOrbit)))
        vTargetBody = math.sqrt(starGM / targetOrbit)
        vRawArrival = vHohmannArrival - vTargetBody
        vEntry = math.sqrt((vRawArrival**2) + ((2*targetGM)/(craftTargetOrbit + targetRad)))
        vCraftTargetOrbit = math.sqrt(targetGM / (craftTargetOrbit + targetRad))
        dv2 = vEntry + vCraftTargetOrbit

        dv = dv1 + dv2
        return dv

    def summateDv(self, *args):
        sumDv = 0
        for i in args:
            sumDv += i
        return sumDv